/*
 * MO_Antibody.h
 *
 *  Created on: Mar 29, 2011
 *      Author: rgreen
 */

#ifndef MO_ANTIBODY_H_
#define MO_ANTIBODY_H_

class MO_Antibody {
public:
	MO_Antibody();
	virtual ~MO_Antibody();
};

#endif /* MO_ANTIBODY_H_ */
