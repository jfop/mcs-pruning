#ifndef UTILSSYSTEM_H_
#define UTILSSYSTEM_H_
#include "Line.h"
#include "Bus.h"
#include <iomanip>

namespace utilssystem{
extern void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem, std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& Buses);
};
#endif
