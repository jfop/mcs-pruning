#ifndef PRINTVECTOR_H_
#define PRINTVECTOR_H_

namespace vectors {
	extern void printVector(std::vector < std::vector < double > >& v, std::string title, int precision = 2);
    extern void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision = 2);
  };
#endif
